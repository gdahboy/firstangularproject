import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plotly-example-component',
  templateUrl: './plotly-example-component.component.html',
  styleUrls: ['./plotly-example-component.component.css']
})
export class PlotlyExampleComponentComponent implements OnInit {

  public graph = {
    data: [
      { x: [1, 2, 3], y: [2, 6, 3], type: 'scatter', mode: 'lines+points', marker: {color: 'red'} }
    ],
    layout: {width: 500, height: 500, title: 'A Fancy Plot'}
  };
  constructor() {}
  ngOnInit() {
  }

}
