import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MyBarChartComponent} from './my-bar-chart/my-bar-chart.component';
import {PlotlyExampleComponentComponent} from './plotly-example-component/plotly-example-component.component';
import {TryFormComponent} from './try-form/try-form.component';

// @ts-ignore
const routes: Routes = [
  {path : 'bar-chart' , component : MyBarChartComponent} ,
  {path : 'ploty' , component : PlotlyExampleComponentComponent},
  {path : 'form' , component : TryFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
