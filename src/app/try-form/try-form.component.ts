import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, NgForm , Validators } from '@angular/forms';
import {Alert} from 'selenium-webdriver';


@Component({
  selector: 'app-try-form',
  templateUrl: './try-form.component.html',
  styleUrls: ['./try-form.component.css']
})
export class TryFormComponent implements OnInit {
  constructor() { }
  form = new FormGroup({
    a: new FormControl('', Validators.required),
    b: new FormControl('', Validators.required),
    c: new FormControl('', Validators.required),
    d: new FormControl('', Validators.required)
  });
  // form2 = new FormGroup({
  //   a : new FormControl('' , Validators.required),
  //   b2 : new FormControl('' , Validators.required)
  // }) ;
  public graph = {
    data: [
      { x: [], y: [], type: 'scatter', mode: 'lines+points', marker: {color: 'red'} }
    ],
    layout: {width: 1000, height: 400, title: 'A Fancy Plot'}
  };
  ngOnInit() {
  }

  onSubmit() {
    alert(JSON.stringify(this.form.value));

    const vara = this.form.value.a ;
    const varb = this.form.value.b ;
    const varc = this.form.value.c ;
    const vard = this.form.value.d ;
    console.log(vara);
    console.log(varb);
    console.log(varc);
    console.log(vard);

    for (let i = 0 ; i < 50 ; i++) {
      this.graph.data[0].x.push(i) ;
      const vary = vara * i * i * i + varb * i * i + varc * i + vard ;
      this.graph.data[0].y.push(vary);
    }

  }
}
