import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-bar-chart',
  templateUrl: './my-bar-chart.component.html',
  styleUrls: ['./my-bar-chart.component.css']
})
export class MyBarChartComponent implements OnInit {

  public barChartOptions =  {
    scaleShowVerticleLines : false,
    responsive : true
  };
  public barChartLabels = ['2006' , '2007' , '2008' , '2009' , '2010' , '2011' , '2012' ] ;
  public barChartType = 'bar' ;
  public barChartLegend = true ;
  public barChartData = [
    {data : [65 , 59 , 80 , 53 , 81 , 45 ] , Label : 'Series A'} ,
    {data : [100 , 34 , 120 , 150 , 70 , 45 ] , Label : 'Series B'}
  ];
  constructor() { }

  ngOnInit() {
  }

}
