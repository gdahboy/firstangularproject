import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ChartsModule} from 'ng2-charts' ;
import * as PlotlyJS from 'plotly.js/dist/plotly.js';
import { PlotlyModule } from 'angular-plotly.js';


import { from } from 'rxjs';
import { MyBarChartComponent } from './my-bar-chart/my-bar-chart.component';
import { PlotlyExampleComponentComponent } from './plotly-example-component/plotly-example-component.component';
import { TryFormComponent } from './try-form/try-form.component';
import { NgForm, NgModel , ReactiveFormsModule } from '@angular/forms';

PlotlyModule.plotlyjs = PlotlyJS;
@NgModule({
  declarations: [
    AppComponent,
    MyBarChartComponent,
    PlotlyExampleComponentComponent,
    TryFormComponent,
    NgForm,
    NgModel,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule ,
    ChartsModule,
    PlotlyModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
